import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../model/priority.dart';

class PrioritiesDropdown extends StatefulWidget {
  List<PriorityTable> priorities;

  Function(PriorityTable) priorityCallback;

  PrioritiesDropdown(
      this.priorities,
      this.priorityCallback, {
        Key? key,
      }) : super(key: key);

  @override
  State<PrioritiesDropdown> createState() => _PrioritiesDropdownState();
}

class _PrioritiesDropdownState extends State<PrioritiesDropdown> {
  var selectedPriority;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<PriorityTable>(
        value: selectedPriority,
        hint: const Text('Select Priority'),
        isExpanded: true,
        items: widget.priorities.map((priorityTable){
          return DropdownMenuItem(
            value: priorityTable,
            child: Text(priorityTable.priorityName!),
          );
        }).toList(),
        onChanged: (PriorityTable? value){
          setState(() {
            widget.priorityCallback(value!);
            selectedPriority = value;
          });
        }
    );
  }
}
