import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../model/category.dart';

class CategoriesDropdown extends StatefulWidget {
  List<CategoryTable> categories;

  Function(CategoryTable) categoryCallback;

  CategoriesDropdown(
      this.categories,
      this.categoryCallback, {
        Key? key,
      }) : super(key: key);

  @override
  State<CategoriesDropdown> createState() => _CategoriesDropdownState();
}

class _CategoriesDropdownState extends State<CategoriesDropdown> {
  var selectedCategory;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<CategoryTable>(
        value: selectedCategory,
        hint: const Text('Select Category'),
        isExpanded: true,
        items: widget.categories.map((categoryTable){
          return DropdownMenuItem(
            value: categoryTable,
            child: Text(categoryTable.categoryName!),
          );
        }).toList(),
        onChanged: (CategoryTable? value){
          setState(() {
            widget.categoryCallback(value!);
            selectedCategory = value;
          });
        }
    );
  }
}
