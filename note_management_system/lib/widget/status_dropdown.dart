import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../model/status.dart';

class StatusDropdown extends StatefulWidget {
  List<StatusTable> status;

  Function(StatusTable) statusCallback;

  StatusDropdown(
      this.status,
      this.statusCallback, {
        Key? key,
      }) : super(key: key);

  @override
  State<StatusDropdown> createState() => _StatusDropdownState();
}

class _StatusDropdownState extends State<StatusDropdown> {
  var selectedStatus;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<StatusTable>(
        value: selectedStatus,
        hint: const Text('Select Status'),
        isExpanded: true,
        items: widget.status.map((statusTable){
          return DropdownMenuItem(
            value: statusTable,
            child: Text(statusTable.statusName!),
          );
        }).toList(),
        onChanged: (StatusTable? value){
          setState(() {
            widget.statusCallback(value!);
            selectedStatus = value;
          });
        }
    );
  }
}
