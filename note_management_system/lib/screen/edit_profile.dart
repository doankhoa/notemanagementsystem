import 'package:flutter/material.dart';
import 'package:nms/screen/dashboard_page.dart';
import '../data/sql_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(const EditProfile());
}

class EditProfile extends StatelessWidget {
  const EditProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return const BuildEditProfile();
  }
}

class BuildEditProfile extends StatefulWidget {
  const BuildEditProfile({super.key});

  @override
  State<BuildEditProfile> createState() => _BuildEditProfileState();
}

class _BuildEditProfileState extends State<BuildEditProfile> {
  final _formKey = GlobalKey<FormState>();

  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.85,
        height: MediaQuery.of(context).size.height * 0.5,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: formEditProfile(),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // final emailPrefs = prefs.getString('email') ?? "";
    setState(() {
      emailController.text = prefs.getString('getEmail') ?? "";
    });
  }

  @override
  void initState() {
    super.initState();
    initValue();
  }

  Widget formEditProfile() => Form(
      key: _formKey,
      child: Expanded(
          child: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            child: const Text(
              'Edit Profile',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.green,
              ),
            ),
          ),
          const Padding(padding: EdgeInsets.only(bottom: 20)),
          TextFormField(
            decoration: const InputDecoration(
                hintText: 'Enter your firstname',
                contentPadding: EdgeInsets.only(left: 25)),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter your first name';
              }
              return null;
            },
            controller: firstNameController,
          ),
          TextFormField(
            decoration: const InputDecoration(
                hintText: 'Enter your last name',
                contentPadding: EdgeInsets.only(left: 25)),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter your last name';
              }
              return null;
            },
            controller: lastNameController,
          ),
          TextFormField(
            decoration: const InputDecoration(
                // hintText: 'email',
                contentPadding: EdgeInsets.only(left: 25)),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter your email';
              }
              return null;
            },
            controller: emailController,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    _updateProfile(firstNameController.text, lastNameController.text);
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Successful')));
                  }
                },
                child: const Text('Update'),
              ),
              const Padding(
                  padding: EdgeInsets.only(top: 150, left: 40, right: 40)),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const HomeForm()));
                  },
                  child: const Text('Home'))
            ],
          )
        ],
      )));

  Future<void> _updateProfile(String firstname, String lastname) async {
    lastname = lastNameController.text;
    firstname = firstNameController.text;
    await SQLHelper.updateProfile(firstname, lastname);
  }
}
