//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../data/sql_helper.dart';
import '../model/note.dart';
import '../model/status.dart';
import 'package:shared_preferences/shared_preferences.dart';


class StatusPage extends StatefulWidget {
  const StatusPage({Key? key}) : super(key: key);

  @override
  State<StatusPage> createState() => _StatusPageState();
}

final _lightColors = [
  Colors.amber.shade300,
  Colors.lightGreen.shade300,
  Colors.lightBlue.shade300,
  Colors.orange.shade300,
  Colors.pinkAccent.shade100,
  Colors.tealAccent.shade100
];

class _StatusPageState extends State<StatusPage> {
  List<StatusTable> status = [];
  List<Note> notes = [];
  bool isLoading = true;
  String emailValue = '';

  final TextEditingController _statusController = TextEditingController();

  @override
  void initState(){
    super.initState();
    _refreshStatus();
    initValue();
  }

  Future<void> _refreshStatus() async {
    final data = await SQLHelper.getAllStatus();
    final data2 = await SQLHelper.getAllNotes();

    setState(() {
      status = data;
      notes = data2;
      isLoading = false;
    });
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // final emailPrefs = prefs.getString('email') ?? "";
    setState(() {
      emailValue = prefs.getString('getEmail') ?? "";
    });
  }

  Future<void> _addStatus() async {
    await SQLHelper.createStatus(StatusTable(
      statusName: _statusController.text,
      email: emailValue,
    ));
    _refreshStatus();
  }

  Future<void> _updateStatus(int id) async {
    await SQLHelper.updateStatus(StatusTable(
      statusId: id,
      statusName: _statusController.text,
    ));

    _refreshStatus();
  }

  Future<void> _deleteStatus(int id) async {

    var result  = status.firstWhere((element) => element.statusId == id);
    var contain = notes
        .where((element) => element.status == result.statusName && element.email== result.email);
    if (contain.isEmpty) {
      await SQLHelper.deleteStatus(id);

      if (!mounted) return;

      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Successfully deleted a status')));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Status has note')));

    }

    _refreshStatus();
  }

  void _showForm (int? id) async {
    if(id != null){
      final existingStatus = status.firstWhere((element) => element.statusId == id);
      _statusController.text = existingStatus.statusName!;
    }

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (_) => Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextFormField(
                controller: _statusController,
                decoration: const InputDecoration(hintText: 'Status'),
              ),
              const SizedBox(height: 10,),
              ElevatedButton(
                onPressed: () async {
                  if (id == null) {
                    await _addStatus();
                  }

                  if (id != null) {
                    await _updateStatus(id);
                  }

                  _statusController.text = '';

                  if(!mounted) return;

                  Navigator.of(context).pop();
                },
                child: Text(id == null ? 'Create New' : 'Update'),
              )
            ],
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Status'),
      ),
      body: Center(
        child: isLoading
            ? const CircularProgressIndicator()
            : status.isEmpty
            ? const Text('No Status')
            : buildStatusPage(),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () => _showForm(null)
      ),
    );
  }

  Widget buildStatusPage() => ListView.builder(
      itemCount: status.length,
      itemBuilder: (context, index) {
        final color = _lightColors[index % _lightColors.length];
        //final time = DateFormat('yyyy-MM-dd hh:mm').format(_categories[index]['createdAt']);
        return Card(
          color: color,
          margin: const EdgeInsets.all(15),
          child: ListTile(
            title: Text(status[index].statusName!),
            // subtitle: Text(
            //   time,
            //   style: TextStyle(color: Colors.grey.shade700),),
            trailing: SizedBox(
              width: 100,
              child: Row(
                children: [
                  IconButton(
                      onPressed: () => _showForm(status[index].statusId),
                      icon: const Icon(Icons.edit)
                  ),
                  IconButton(
                      onPressed: () => _deleteStatus(status[index].statusId!),
                      icon: const Icon(Icons.delete)
                  ),
                ],
              ),
            ),
            // onTap: () async {
            //   await Navigator.of(context).push(MaterialPageRoute(builder: (context) => CategoryDetailPage(categoryId: _categories[index]['categoryId']!)));
            // }
          ),
        );
      }
  );
}

