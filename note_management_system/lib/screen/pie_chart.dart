import 'package:flutter/material.dart';
import '../data/sql_helper.dart';
import 'package:nms/model/status.dart';
import 'package:pie_chart/pie_chart.dart';

void main(){
  runApp(const Home());
}

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Pie Chart',
      home: BuildPieChart(),
    );
  }
}

class BuildPieChart extends StatefulWidget {

  const BuildPieChart({super.key});

  @override
  State<BuildPieChart> createState() => _BuildPieChartState();
}

class _BuildPieChartState extends State<BuildPieChart> {
  Map<String, double> dataMap = {
    "Done": 5,
    "Spending": 3,
    "Processing": 4,
  };


  @override
  Widget build(BuildContext context) {
    return Center(
        child: PieChart(
          dataMap: dataMap,
          chartRadius: MediaQuery.of(context).size.width / 1.7,
          legendOptions: const LegendOptions(
            legendPosition: LegendPosition.bottom,
          ),
          chartValuesOptions: const ChartValuesOptions(
            showChartValuesInPercentage: true,
          ),

        ),
      );
  }

}