import 'package:flutter/material.dart';
import 'package:nms/screen/category_page.dart';
import 'package:nms/screen/change_password.dart';
import 'package:nms/screen/edit_profile.dart';
import 'package:nms/screen/note_page.dart';
import 'package:nms/screen/pie_chart.dart';
import 'package:nms/screen/priority_page.dart';
import 'package:nms/screen/status_page.dart';



class HomeForm extends StatelessWidget {
  const HomeForm({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var currentPage = DrawerSections.home;
  String updateTitle = 'Dashboard';

  @override
  Widget build(BuildContext context) {
    var container;
    if (currentPage == DrawerSections.home) {
      container = const BuildPieChart();
    } else if (currentPage == DrawerSections.category) {
      container = const CategoryPage();
    } else if (currentPage == DrawerSections.priority) {
      container = const PriorityPage();
    } else if (currentPage == DrawerSections.status) {
      container = const StatusPage();
    } else if (currentPage == DrawerSections.note) {
      container = const NotePage();
    } else if (currentPage == DrawerSections.edit_profile) {
      container = const EditProfile();
    } else if (currentPage == DrawerSections.change_password) {
      container = const ChangePassword();
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text(updateTitle),
      ),
      body: container,
      drawer: Drawer(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const UserAccountsDrawerHeader(
                accountName: Text('Kim Ngan'),
                accountEmail: Text('kimngan@gmail.com'),
                currentAccountPicture: CircleAvatar(
                  child: ClipOval(
                    child: Image(image: AssetImage('images/husky.png')),
                  ),
                ),
              ),

              MyDrawerList(),
            ],
          ),
        ),
      ),
    );
  }

  Widget MyDrawerList() {
    return Container(
      padding: const EdgeInsets.only(
        top: 10,
      ),
      child: Column(
        // shows the list of menu drawer
        children: [
          menuItem(1, "Home", Icons.camera_alt,
              currentPage == DrawerSections.home ? true : false),
          menuItem(2, "Category", Icons.collections,
              currentPage == DrawerSections.category ? true : false),
          menuItem(3, "Priority", Icons.video_collection,
              currentPage == DrawerSections.priority ? true : false),
          menuItem(4, "Status", Icons.build,
              currentPage == DrawerSections.status ? true : false),
          menuItem(5, "Note", Icons.build,
              currentPage == DrawerSections.note ? true : false),
          const Divider(),
          const Text(
            'Account',
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Colors.black26,
            ),
          ),
          menuItem(6, "Edit Profile", Icons.share,
              currentPage == DrawerSections.edit_profile ? true : false),
          menuItem(7, "Change Password", Icons.send,
              currentPage == DrawerSections.change_password ? true : false),
        ],
      ),
    );
  }

  Widget menuItem(int id, String title, IconData icon, bool selected) {
    return Material(
      color: selected ? Colors.tealAccent : Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
          setState(() {
            if (id == 1) {
              currentPage = DrawerSections.home;
              updateTitle = 'Dashboard';
            } else if (id == 2) {
              currentPage = DrawerSections.category;
              updateTitle = 'Category';
            } else if (id == 3) {
              currentPage = DrawerSections.priority;
              updateTitle = 'Priority';
            } else if (id == 4) {
              currentPage = DrawerSections.status;
              updateTitle = 'Status';
            } else if (id == 5) {
              currentPage = DrawerSections.note;
              updateTitle = 'Note';
            } else if (id == 6) {
              currentPage = DrawerSections.edit_profile;
              updateTitle = 'Edit Profile';
            } else if (id == 7) {
              currentPage = DrawerSections.change_password;
              updateTitle = 'Change Password';
            }
          });
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 15.0, left: 0, bottom: 15),
          child: Row(
            children: [
              Expanded(
                child: Icon(
                  icon,
                  size: 20,
                  color: Colors.teal,
                ),
              ),
              Expanded(
                flex: 4,
                child: Text(
                  title,
                  style: const TextStyle(
                    color: Colors.teal,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum DrawerSections {
  home,
  category,
  priority,
  status,
  note,
  edit_profile,
  change_password,
}
