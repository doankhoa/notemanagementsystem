//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../data/sql_helper.dart';
import '../model/note.dart';
import '../model/priority.dart';
import 'package:shared_preferences/shared_preferences.dart';


class PriorityPage extends StatefulWidget {
  const PriorityPage({Key? key}) : super(key: key);

  @override
  State<PriorityPage> createState() => _PriorityPageState();
}

final _lightColors = [
  Colors.amber.shade300,
  Colors.lightGreen.shade300,
  Colors.lightBlue.shade300,
  Colors.orange.shade300,
  Colors.pinkAccent.shade100,
  Colors.tealAccent.shade100
];

class _PriorityPageState extends State<PriorityPage> {
  List<PriorityTable> priorities = [];
  List<Note> notes = [] ;
  bool isLoading = true;
  String emailValue = '';

  final TextEditingController _priorityController = TextEditingController();

  @override
  void initState(){
    super.initState();
    _refreshPriority();
    initValue();
  }

  Future<void> _refreshPriority() async {
    final data = await SQLHelper.getAllPriorities();
    final data2 = await SQLHelper.getAllNotes();

    setState(() {
      priorities = data;
      notes = data2;
      isLoading = false;
    });
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // final emailPrefs = prefs.getString('email') ?? "";
    setState(() {
      emailValue = prefs.getString('getEmail') ?? "";
    });
  }

  Future<void> _addPriority() async {
    await SQLHelper.createPriority(PriorityTable(
      priorityName: _priorityController.text,
      email: emailValue,
    ));
    _refreshPriority();
  }

  Future<void> _updatePriority(int id) async {
    await SQLHelper.updatePriority(PriorityTable(
      priorityId: id,
      priorityName: _priorityController.text,
    ));

    _refreshPriority();
  }

  Future<void> _deletePriority(int id) async {

    var result  = priorities.firstWhere((element) => element.priorityId == id);
    var contain = notes
        .where((element) => element.priority == result.priorityName && element.email== result.email);
    if (contain.isEmpty) {
      await SQLHelper.deletePriority(id);

      if (!mounted) return;

      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Successfully deleted a priority')));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Priority has note')));

    }

    _refreshPriority();
  }

  void _showForm (int? id) async {
    if(id != null){
      final existingPriority = priorities.firstWhere((element) => element.priorityId== id);
      _priorityController.text = existingPriority.priorityName!;
    }

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (_) => Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextFormField(
                controller: _priorityController,
                decoration: const InputDecoration(hintText: 'Priority'),
              ),
              const SizedBox(height: 10,),
              ElevatedButton(
                onPressed: () async {
                  if (id == null) {
                    await _addPriority();
                  }

                  if (id != null) {
                    await _updatePriority(id);
                  }

                  _priorityController.text = '';

                  if(!mounted) return;

                  Navigator.of(context).pop();
                },
                child: Text(id == null ? 'Create New' : 'Update'),
              )
            ],
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Priority'),
      ),
      body: Center(
        child: isLoading
            ? const CircularProgressIndicator()
            : priorities.isEmpty
            ? const Text('No Priority')
            : buildPriorityPage(),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () => _showForm(null)
      ),
    );
  }

  Widget buildPriorityPage() => ListView.builder(
      itemCount: priorities.length,
      itemBuilder: (context, index) {
        final color = _lightColors[index % _lightColors.length];
        //final time = DateFormat('yyyy-MM-dd hh:mm').format(_categories[index]['createdAt']);
        return Card(
          color: color,
          margin: const EdgeInsets.all(15),
          child: ListTile(
            title: Text(priorities[index].priorityName!),
            // subtitle: Text(
            //   time,
            //   style: TextStyle(color: Colors.grey.shade700),),
            trailing: SizedBox(
              width: 100,
              child: Row(
                children: [
                  IconButton(
                      onPressed: () => _showForm(priorities[index].priorityId),
                      icon: const Icon(Icons.edit)
                  ),
                  IconButton(
                      onPressed: () => _deletePriority(priorities[index].priorityId!),
                      icon: const Icon(Icons.delete)
                  ),
                ],
              ),
            ),
            // onTap: () async {
            //   await Navigator.of(context).push(MaterialPageRoute(builder: (context) => CategoryDetailPage(categoryId: _categories[index]['categoryId']!)));
            // }
          ),
        );
      }
  );
}

