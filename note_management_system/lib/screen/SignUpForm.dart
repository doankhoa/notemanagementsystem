import 'package:flutter/material.dart';
import '../screen/SignInForm.dart';

import '../data/sql_helper.dart';
import '../model/userModel.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({super.key});

  @override
  SignUpFormState createState() => SignUpFormState();
// TODO: implement createState

}

class SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  final _userEmail = TextEditingController();
  final _userPassword = TextEditingController();
  final _userRePassword = TextEditingController();


  Future<void> insertItem() async {
    await SQLHelper.createItem(Account(
      email: _userEmail.text,
      password: _userPassword.text,
      firstname: null,
      lastname: null,
    ));
  }

  // Future<List<Map<String, dynamic>>> _Email(Account account) async{
  //   final data = await SQLHelperAccount.checkEmail(account);
  //   return data;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('NoteManagementSystem'),
          backgroundColor: Colors.green,
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Padding(
                      padding:
                          EdgeInsets.only(top: 80, left: 160, right: 10)),
                  const Text(
                    "Sign Up Form",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      controller: _userEmail,
                      decoration: const InputDecoration(
                          hintText: 'Enter your Email',
                          contentPadding: EdgeInsets.only(left: 25)),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email is Required';
                        }

                        if (!RegExp(
                                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                            .hasMatch(value)) {
                          return 'Please enter a valid email Address';
                        }

                        return null;
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                        controller: _userPassword,
                        obscureText: true, //hide pass
                        obscuringCharacter: "*",
                        decoration: const InputDecoration(
                            hintText: 'Enter your Password',
                            contentPadding: EdgeInsets.only(left: 25)),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter your password';
                          } else if (value.length < 8) {
                            return "Length of password's character must be 8 or greater";
                          }
                          return null;
                        }),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                        obscureText: true, //hide pass
                        obscuringCharacter: "*",
                        controller: _userRePassword,
                        decoration: const InputDecoration(
                            hintText: 'Enter your Password',
                            contentPadding: EdgeInsets.only(left: 25)),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please confirm your password';
                          } else if (value != _userPassword.text) {
                            return "Password miss";
                          }
                          return null;
                        }),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.black12)),
                        onPressed: ()async {
                          if (_formKey.currentState!.validate()) {
                           insertItem();
                           ScaffoldMessenger.of(context).showSnackBar(
                               const SnackBar(
                                   content: Text('Sign up successful')));

                          _userEmail.text = '';
                          _userPassword.text = '';
                          _userRePassword.text='';
                        }},
                        child: const Text('Sign Up',
                            style: TextStyle(color: Colors.black))
                        ),


                      const Padding(
                          padding: EdgeInsets.only(
                              top: 100, left: 160, right: 10)),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => const SignInForm()));
                          },
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.black12),
                          ),
                          child: const Text('Sign In',
                              style: TextStyle(color: Colors.black)))
                    ],
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
