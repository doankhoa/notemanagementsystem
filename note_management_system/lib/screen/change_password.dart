import 'package:flutter/material.dart';
import 'package:nms/screen/dashboard_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../data/sql_helper.dart';

void main() {
  runApp(const ChangePassword());
}

class ChangePassword extends StatelessWidget {
  const ChangePassword({super.key});

  @override
  Widget build(BuildContext context) {
    return const BuildChangePassword();
  }
}

class BuildChangePassword extends StatefulWidget {
  const BuildChangePassword({super.key});

  @override
  State<BuildChangePassword> createState() => _BuildChangePasswordState();
}

class _BuildChangePasswordState extends State<BuildChangePassword> {
  final _formKey = GlobalKey<FormState>();

  final currentPasswordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final confirmPasswordController = TextEditingController();



  @override
  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.85,
          height: MediaQuery.of(context).size.height * 0.5,
          // decoration: BoxDecoration(
          //     borderRadius: BorderRadius.circular(10),
          //     boxShadow: const [
          //       BoxShadow(color: Colors.white38, blurRadius: 8, spreadRadius: 5)
          //     ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: FormChangePassword(),
              ),
            ],
          ),
        ),
      );
  }

  Widget FormChangePassword() => Form(
      key: _formKey,
      child: Expanded(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                child: const Text(
                  'Change Your Password',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.green,
                  ),
                ),
              ),
              const Padding(padding: EdgeInsets.only(bottom: 20)),
              TextFormField(
                obscureText: true, //hide pass
                obscuringCharacter: "*",
                decoration: const InputDecoration(
                    hintText: 'Enter current password',
                    contentPadding: EdgeInsets.only(left: 25)),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your current password';
                  }
                  return null;
                },
                controller: currentPasswordController,
              ),
              TextFormField(
                obscureText: true, //hide pass
                obscuringCharacter: "*",
                decoration: const InputDecoration(
                    hintText: 'Enter your new password',
                    contentPadding: EdgeInsets.only(left: 25)),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your password';
                  } else if (value.length < 8) {
                    return "Length of password's character must be 8 or greater";
                  }
                  return null;
                },
                controller: newPasswordController,
              ),
              TextFormField(
                obscureText: true, //hide pass
                obscuringCharacter: "*",
                decoration: const InputDecoration(
                    hintText: 'Enter password again',
                    contentPadding: EdgeInsets.only(left: 25)),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please confirm your password';
                  } else if (value != newPasswordController.text) {
                    return "Password miss";
                  }
                  return null;
                },
                controller: confirmPasswordController,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () async {
                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      final passwordPrefs = prefs.getString('getPassword') ?? "";
                      if (_formKey.currentState!.validate() && passwordPrefs == currentPasswordController.text) {
                        _updatePassword(newPasswordController.text);
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Successful')));
                      }
                      else{
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text('Password fail')));
                        currentPasswordController.text = '';
                      }
                    },
                    child: const Text('Change'),
                  ),
                  const Padding(
                      padding: EdgeInsets.only(top: 150, left: 40, right: 40)),
                  ElevatedButton(onPressed: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const HomeForm()));
                  }, child: const Text('Home'))
                ],
              ),
            ],
          )
      )
  );

  Future<void> _updatePassword(String newPassword) async{
    newPassword = newPasswordController.text;
    await SQLHelper.updatePassword(newPassword);
  }
}
