
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nms/screen/dashboard_page.dart';
import '../data/sql_helper.dart';
import '../model/userModel.dart';
import '../screen/SignUpForm.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../model/constance.dart';

class SignInForm extends StatefulWidget{
  const SignInForm({super.key});

  @override
  // TODO: implement createState
  LoginFormState createState() => LoginFormState();
}

class LoginFormState extends State<SignInForm>{

  final _formKey = GlobalKey<FormState>();

  final userEmail = TextEditingController();
  final getEmail = TextEditingController();
  final getPassword = TextEditingController();
  final userPassword = TextEditingController();

  bool isChecked = false;

  bool isSignIn = false;


  Future<List<Map<String, dynamic>>> _login(Account account) async{
    final data = await SQLHelper.checkAccount(account);
    return data;
  }
  void setUserPass() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(Constance.getEmail, userEmail.text);
    prefs.setString(Constance.getPassword, userPassword.text);
  }

  void setRemember (bool flag) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(flag){

      prefs.setString(Constance.email, userEmail.text);
      prefs.setString(Constance.password, userPassword.text);
    }else{
      await prefs.clear();

   }
    prefs.setBool(Constance.remember, flag);
  }

  Future<void> initValue() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      getEmail.text = preferences.getString(Constance.getEmail)?? "";
      getPassword.text=preferences.getString(Constance.password)?? "";
      userEmail.text = preferences.getString(Constance.email) ?? "";
      userPassword.text=preferences.getString(Constance.password) ?? "";
      isChecked =preferences.getBool(Constance.remember)?? false;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isSignIn = true;
    initValue();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: const Text('NoteManagementSystem'),
          backgroundColor: Colors.green,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (_) => const SignUpForm()));
          },
          backgroundColor: Colors.red,
          child: const Icon(Icons.add),
        ),
        body: Form(
          key: _formKey,
          child :SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,

                children: [
                  const Padding(
                      padding: EdgeInsets.only(top: 80, left: 160, right: 10)),
                  const Text("Note Management System",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      controller : userEmail,
                      decoration: const InputDecoration(
                          hintText: 'Enter your Email',
                          contentPadding: EdgeInsets.only(left: 25)),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email is Required';
                        }

                        if (!RegExp(
                            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                            .hasMatch(value)) {
                          return 'Please enter a valid email Address';
                        }

                        return null;
                      },
                    ),
                  ),

                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                        controller : userPassword,
                        obscureText: true, //hide pass
                        obscuringCharacter: "*",
                        decoration: const InputDecoration(
                            hintText: 'Enter your Password',
                            contentPadding: EdgeInsets.only(left: 25)),
                        validator:(value){
                          if(value == null || value.isEmpty){
                            return 'Please enter your password';
                          }
                          else if(value.length<8){
                            return "Length of password's character must be 8 or greater";
                          }
                          return null;
                        }
                    ),
                  ),
                  Row(children: [
                    const Padding(padding: EdgeInsets.only(left: 15)),
                    Checkbox(
                      checkColor: Colors.white,
                      value: isChecked,
                      onChanged: (value) {
                        setState(() {
                          isChecked = value!;
                        });
                      },
                    ),
                    const Text('Remember me'),
                  ]),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.black12)),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            var account =
                            Account(email: userEmail.text, password: userPassword.text);
                            List<Map<String, dynamic>> data = await _login(account);
                            if (data.isEmpty && mounted) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(content: Text('Sign in failed')));
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                      content: Text('Sign in Successfully')));
                              Navigator.push(
                                  context, MaterialPageRoute(
                                  builder: (_) => const HomeForm()));
                              setRemember(isChecked);
                              setUserPass();
                            }
                          }
                        },
                        child: const Text('Sign In',
                            style: TextStyle(color: Colors.black)),
                      ),
                      const Padding(
                          padding: EdgeInsets.only(
                              top: 100, left: 160, right: 10)),
                      ElevatedButton(
                          onPressed: () {
                            SystemNavigator.pop();
                          },
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors
                                .black12),
                          ),
                          child:
                          const Text('Exit', style: TextStyle(color: Colors
                              .black))
                      ),

                    ],
                  ),
                ],

              ),

            ),
          ),
        )
    );
  }
}
