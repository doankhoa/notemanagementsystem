import 'package:flutter/material.dart';

void main() => runApp(
    const MaterialApp(
      home: HomePage (),
    )
);

class HomePage  extends StatelessWidget {
  const HomePage({super.key});


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: const Text('NoteManagementSystem'),
        ),
        body: SizedBox(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: const <Widget>[
                    Text("Sign Up Form",
                      style: TextStyle(color: Colors.black, fontSize: 20),)
                  ],
                ),
              ),
              Expanded(child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(10),
                    // decoration: BoxDecoration(
                    //     border: Border(bottom: BorderSide(color: Colors.grey[200]))
                    // ),
                    child: const TextField(
                      decoration: InputDecoration(
                          hintText: "Enter your email",
                          hintStyle: TextStyle(color: Colors.grey),
                          border: InputBorder.none
                      ),
                    ),
                  ),
                  const Divider(
                    height: 2,
                    color: Colors.black,
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    // decoration: BoxDecoration(
                    //     border: Border(bottom: BorderSide(color: Colors.grey[200]))
                    // ),
                    child: const TextField(
                      decoration: InputDecoration(
                          hintText: "Enter your password",
                          hintStyle: TextStyle(color: Colors.grey),
                          border: InputBorder.none
                      ),
                    ),
                  ),
                  const Divider(
                    height: 2,
                    color: Colors.black,
                  ),

                  Container(
                    padding: const EdgeInsets.all(10),
                    // decoration: BoxDecoration(
                    //     border: Border(bottom: BorderSide(color: Colors.grey[200]))
                    // ),
                    child: const TextField(
                      decoration: InputDecoration(
                          hintText: "Confirm password",
                          hintStyle: TextStyle(color: Colors.grey),
                          border: InputBorder.none
                      ),
                    ),
                  ),
                  const Divider(
                    height: 2,
                    color: Colors.black,
                  ),

                  const ListTile(
                    leading: Text('Sign up', style: TextStyle(
                      color: Colors.blue, fontWeight: FontWeight.w800,)),
                    trailing: Text('Sign in',
                        style: TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.w800,)),
                  )
                ],
              )
              )
            ],
          ),
        )
    );
  }

}

