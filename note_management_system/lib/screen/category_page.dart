import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../data/sql_helper.dart';
import '../model/category.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/note.dart';


class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

final _lightColors = [
  Colors.amber.shade300,
  Colors.lightGreen.shade300,
  Colors.lightBlue.shade300,
  Colors.orange.shade300,
  Colors.pinkAccent.shade100,
  Colors.tealAccent.shade100
];

class _CategoryPageState extends State<CategoryPage> {
  List<CategoryTable> categories = [] ;
  List<Note> notes = [] ;
  bool isLoading = true;
  String emailValue = '';

  final TextEditingController _categoryController = TextEditingController();

  @override
  void initState(){
    super.initState();
    _refreshCategory();
    initValue();
  }

  Future<void> _refreshCategory() async {
    final data = await SQLHelper.getAllCategories();
    final data2 = await SQLHelper.getAllNotes();

    setState(() {
      categories = data;
      notes = data2;
      isLoading = false;
    });
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // final emailPrefs = prefs.getString('email') ?? "";
    setState(() {
      emailValue = prefs.getString('getEmail') ?? "";
    });
  }

  Future<void> _addCategory() async {
    await SQLHelper.createCategory(CategoryTable(
      categoryName: _categoryController.text,
      email: emailValue,
    ));
    _refreshCategory();
  }

  Future<void> _updateCategory(int id) async {
    await SQLHelper.updateCategory(CategoryTable(
      categoryId: id,
      categoryName: _categoryController.text,
    ));

    _refreshCategory();
  }

  Future<void> _deleteCategory(int id) async {

    var result  = categories.firstWhere((element) => element.categoryId == id);
    var contain = notes
        .where((element) => element.category == result.categoryName && element.email== result.email);
    if (contain.isEmpty) {
      await SQLHelper.deleteCategory(id);

      if (!mounted) return;

      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Successfully deleted a category')));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Category has note')));

    }

    _refreshCategory();
  }

  void _showForm (int? id) async {
    if(id != null){
      final existingCategory = categories.firstWhere((element) => element.categoryId == id);
      _categoryController.text = existingCategory.categoryName!;
    }

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (_) => Container(
          padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextFormField(
                controller: _categoryController,
                decoration: const InputDecoration(hintText: 'Category'),
              ),
              const SizedBox(height: 10,),
              ElevatedButton(
                onPressed: () async {
                  if (id == null) {
                    await _addCategory();
                  }

                  if (id != null) {
                    await _updateCategory(id);
                  }

                  _categoryController.text = '';

                  if(!mounted) return;

                  Navigator.of(context).pop();
                },
                child: Text(id == null ? 'Create New' : 'Update'),
              )
            ],
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Category'),
      ),
      body: Center(
        child: isLoading
            ? const CircularProgressIndicator()
            : categories.isEmpty
            ? const Text('No Category')
            : buildCategoryPage(),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () => _showForm(null)
      ),
    );
  }

  Widget buildCategoryPage() => ListView.builder(
      itemCount: categories.length,
      itemBuilder: (context, index) {
        final color = _lightColors[index % _lightColors.length];
        //final time = DateFormat('yyyy-MM-dd hh:mm').format(_categories[index]['createdAt']) as DateTime;
        return Card(
          color: color,
          margin: const EdgeInsets.all(15),
          child: ListTile(
            title: Text(categories[index].categoryName!),
            //subtitle: Text(
              //time.toString(),
              //style: TextStyle(color: Colors.grey.shade700),),
            trailing: SizedBox(
              width: 100,
              child: Row(
                children: [
                  IconButton(
                      onPressed: () => _showForm(categories[index].categoryId),
                      icon: const Icon(Icons.edit)
                  ),
                  IconButton(
                      onPressed: () => _deleteCategory(categories[index].categoryId!),
                      icon: const Icon(Icons.delete)
                  ),
                ],
              ),
            ),
            // onTap: () async {
            //   await Navigator.of(context).push(MaterialPageRoute(builder: (context) => CategoryDetailPage(categoryId: _categories[index]['categoryId']!)));
            // }
          ),
        );
      }
  );
}

