// import 'package:flutter/material.dart';
// import '../screen/note_page.dart';
// import '../screen/category_page.dart';
// import '../screen/status_page.dart';
// import '../screen/priority_page.dart';
//
// class HomeForm extends StatefulWidget {
//   const HomeForm({super.key});
//
//   @override
//   State<HomeForm> createState() => _HomeFormState();
// }
//
// class _HomeFormState extends State<HomeForm> {
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Welcome to Flutter',
//       home: Scaffold(
//         appBar: AppBar(
//           title: const Text('Welcome to Flutter'),
//         ),
//         body: Center(
//           child: ListView(
//             children: [
//               IconButton(
//                   icon: const Icon(Icons.one_k),
//                   onPressed: (){
//                     Navigator.of(context).push(MaterialPageRoute(builder: (context) => const NotePage()));
//                   }),
//               IconButton(
//                   icon: const Icon(Icons.two_k),
//                   onPressed: (){
//                     Navigator.of(context).push(MaterialPageRoute(builder: (context) => const CategoryPage()));
//                   }),
//               IconButton(
//                   icon: const Icon(Icons.three_k),
//                   onPressed: (){
//                     Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PriorityPage()));
//                   }),
//               IconButton(
//                   icon: const Icon(Icons.four_k),
//                   onPressed: (){
//                     Navigator.of(context).push(MaterialPageRoute(builder: (context) => const StatusPage()));
//                   }),
//             ],
//           )
//
//         )
//       ),
//     );
//   }
// }