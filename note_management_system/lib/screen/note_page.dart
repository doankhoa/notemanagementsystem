//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../data/sql_helper.dart';
import '../model/note.dart';
import '../model/category.dart';
import '../model/priority.dart';
import '../model/status.dart';
import '../widget/categories_dropdown.dart';
import '../widget/priorities_dropdown.dart';
import '../widget/status_dropdown.dart';
import 'package:shared_preferences/shared_preferences.dart';



class NotePage extends StatefulWidget {
  const NotePage({Key? key}) : super(key: key);

  @override
  State<NotePage> createState() => _NotePageState();
}

final _lightColors = [
  Colors.amber.shade300,
  Colors.lightGreen.shade300,
  Colors.lightBlue.shade300,
  Colors.orange.shade300,
  Colors.pinkAccent.shade100,
  Colors.tealAccent.shade100
];


class _NotePageState extends State<NotePage> {
  List<Note> notes = [];
  bool isLoading = true;
  CategoryTable? _selectedCategory;
  PriorityTable? _selectedPriority;
  StatusTable? _selectedStatus;
  String emailValue = '';
  DateTime _date = DateTime.now();
  final _noteController = TextEditingController();
  final _dateController = TextEditingController();

  categoryCallback(selectedCategory){
    setState(() {
      _selectedCategory = selectedCategory;
      print(_selectedCategory?.categoryName);
    });
  }

  priorityCallback(selectedPriority){
    setState(() {
      _selectedPriority = selectedPriority;
      print(_selectedPriority?.priorityName);
    });
  }

  statusCallback(selectedStatus){
    setState(() {
      _selectedStatus = selectedStatus;
      print(_selectedStatus?.statusName);
    });
  }

  @override
  void initState(){
    super.initState();
    _refreshNote();
    initValue();
  }

  Future<void> _refreshNote() async {
    final data = await SQLHelper.getAllNotes();

    setState(() {
      notes = data;
      isLoading = false;
    });
  }

  Future<void> initValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      emailValue = prefs.getString('getEmail') ?? "";
    });
  }

  Future<void> _addNote() async {
    await SQLHelper.createNote(Note(
        noteName: _noteController.text,
        email: emailValue,
        planDate: _dateController.text,
        createdAt: DateFormat('dd/MM/yyyy').format(DateTime.now()),
        category: _selectedCategory?.categoryName,
        priority: _selectedPriority?.priorityName,
        status: _selectedStatus?.statusName
    ));
    _refreshNote();
  }

  Future<void> _updateNote(int id) async {
    await SQLHelper.updateNote(Note(
        noteId: id,
        noteName: _noteController.text,
        email: emailValue,
        planDate: _dateController.text,
        createdAt: DateFormat('dd/MM/yyyy').format(DateTime.now()),
        category: _selectedCategory?.categoryName,
        priority: _selectedPriority?.priorityName,
        status: _selectedStatus?.statusName
    ));

    _refreshNote();
  }

  Future<void> _deleteNote(int id) async {
    await SQLHelper.deleteNote(id);

    if(!mounted) return;

    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Successfully deleted note'))
    );

    _refreshNote();
  }

  void _showForm (int? id) async {
    if(id != null){
      final existingNote = notes.firstWhere((element) => element.noteId == id);
      _noteController.text = existingNote.noteName!;
      _dateController.text = existingNote.planDate!;
    }

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (_) => Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextFormField(
                controller: _noteController,
                decoration: const InputDecoration(hintText: 'Note'),
              ),
              const SizedBox(height: 10,),
              FutureBuilder<List<CategoryTable>>(

                future: SQLHelper.getAllCategories(),
                builder: (context, snapshot){
                  return snapshot.hasData
                      ? CategoriesDropdown(snapshot.data!, categoryCallback)
                      : const Text('No categories');
                },
              ),
              FutureBuilder<List<PriorityTable>>(
                future: SQLHelper.getAllPriorities(),
                builder: (context, snapshot){
                  return snapshot.hasData
                      ? PrioritiesDropdown(snapshot.data!, priorityCallback)
                      : const Text('No priorities');
                },
              ),
              FutureBuilder<List<StatusTable>>(
                future: SQLHelper.getAllStatus(),
                builder: (context, snapshot){
                  return snapshot.hasData
                      ? StatusDropdown(snapshot.data!, statusCallback)
                      : const Text('No status');
                },
              ),
              TextField(
                controller: _dateController,
                decoration: InputDecoration(
                    labelText: 'Date',
                    hintText: 'Pick a Date',
                    prefixIcon: InkWell(
                      onTap: () async {
                        DateTime? newDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1900),
                          lastDate: DateTime(2100),
                        );
                        if (newDate != null){
                          setState(() {
                            _date = newDate;
                            _dateController.text= '${_date.year}/${_date.month}/${_date.day}';
                          });
                        }
                      },
                      child: const Icon(Icons.calendar_today),
                    )
                ),
              ),

              ElevatedButton(
                onPressed: () async {
                  if (id == null) {
                    await _addNote();
                  }

                  if (id != null) {
                    await _updateNote(id);
                  }

                  _noteController.text = '';
                  _dateController.text = '';

                  if(!mounted) return;

                  Navigator.of(context).pop();
                },
                child: Text(id == null ? 'Create New' : 'Update'),
              )
            ],
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Note'),
      ),
      body: Center(
        child: isLoading
            ? const CircularProgressIndicator()
            : notes.isEmpty
            ? const Text('No Note')
            : buildNotePage(),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () => _showForm(null)
      ),
    );
  }

  Widget buildNotePage() => ListView.builder(
      itemCount: notes.length,
      itemBuilder: (context, index) {
        final color = _lightColors[index % _lightColors.length];
        //final time = DateFormat('yyyy-MM-dd hh:mm').format(_categories[index]['createdAt']) as DateTime;
        return Card(
          color: color,
          margin: const EdgeInsets.all(15),
          child: ListTile(
            title: Text(
              notes[index].noteName!,
              style: const TextStyle(
                fontSize: 20,
              ),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Category: ${notes[index].category!}'),
                Text('Priority: ${notes[index].priority!}'),
                Text('Status: ${notes[index].status!}'),
                Text('Plan Date: ${notes[index].planDate!}')
              ],
            ),
            trailing: SizedBox(
              width: 100,
              child: Row(
                children: [
                  IconButton(
                      onPressed: () => _showForm(notes[index].noteId),
                      icon: const Icon(Icons.edit)
                  ),
                  IconButton(
                      onPressed: () => _deleteNote(notes[index].noteId!),
                      icon: const Icon(Icons.delete)
                  ),
                ],
              ),
            ),
          ),
        );
      }
  );
}

