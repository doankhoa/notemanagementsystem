
import 'package:nms/model/user.dart';
import 'package:sqflite/sqflite.dart';

class SQLHelperUser {
  static Future<void> createUserTable(Database database) async {
    await database.execute('''CREATE TABLE User(
      idUser INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      email TEXT,
      password TEXT,
    )''');
  }

  static Future<Database> db() async {
    return openDatabase(
        'user.db',
        version: 1,
        onCreate: (Database database, int version) async {
          await createUserTable(database);
        }
    );
  }

  static Future<int> createUser(User user) async {
    final db = await SQLHelperUser.db();
    final iduser = await db.insert('Users', user.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return iduser;
  }

  static Future<List<Map<String, dynamic>>> getUser() async {
    final db = await SQLHelperUser.db();

    return db.query('Users', orderBy: 'iduser');
  }


  static Future<int> updateItem(User user) async {
    final db = await SQLHelperUser.db();

    final result = await db
        .update(
        'Users', user.toMap(), where: 'iduser = ?', whereArgs: [user.idUser]);
    return result;
  }
}
