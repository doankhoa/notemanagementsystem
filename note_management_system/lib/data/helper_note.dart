import 'package:nms/model/note.dart';
import 'package:sqflite/sqflite.dart';

class SQLHelperUser {
  static Future<void> createNoteTable(Database database) async {
    await database.execute('''CREATE TABLE Note(
      idNote INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      idUser INTERGER,
      Name TEXT,
      Category TEXT,
      Priority TEXT,
      Status TEXT,
      PlanDate TEXT,
      createAT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP 
    )''');
  }
  
}