import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import '../model/category.dart';
import '../model/priority.dart';
import '../model/status.dart';
import '../model/note.dart';
import '../model/userModel.dart';

class SQLHelper {
  static Future<void> createTable(Database database) async {
    await database.execute('''CREATE TABLE categories(
      categoryId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      categoryName TEXT,
      email TEXT,
      createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )''');

    await database.execute('''CREATE TABLE priorities(
      priorityId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      priorityName TEXT,
      email TEXT,
      createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )''');

    await database.execute('''CREATE TABLE status(
      statusId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      statusName TEXT,
      email TEXT,
      createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )''');

    await database.execute('''CREATE TABLE accounts(
      id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      email TEXT NOT NULL,
      password TEXT NOT NULL,
      firstname TEXT,
      lastname TEXT,
      createAT TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )''');

    await database.execute('''CREATE TABLE notes(
      noteId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
      noteName TEXT,
      email TEXT,
      planDate TIMESTAMP,
      createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      FK_notes_category String NOT NULL,
      FK_notes_priority String NOT NULL,
      FK_notes_status String NOT NULL,
      FOREIGN KEY (FK_notes_category) REFERENCES categories (categoryName),
      FOREIGN KEY (FK_notes_priority) REFERENCES priorities (priorityName),
      FOREIGN KEY (FK_notes_status) REFERENCES status (statusName)
    )''');
  }

  static Future<Database> db() async {
    return openDatabase(
      'database.db',
      version: 1,
      onCreate: (Database database, int version) async {
        await createTable(database);
      },
    );
  }

  //create new category item
  static Future<int> createCategory(CategoryTable categoryTable) async {
    final db = await SQLHelper.db();
    final categoryId = await db.insert('categories', categoryTable.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return categoryId;
  }

  //Read all category items List-> obj
  static Future<List<CategoryTable>> getAllCategories() async {
    final db = await SQLHelper.db();
    List<Map<String, dynamic>> allCategoryRows = await db.query('categories');
    List<CategoryTable> categories =
    allCategoryRows.map((categoryTable) => CategoryTable.fromMap(categoryTable)).toList();
    return categories;
  }


  //Update category item by id
  static Future<int> updateCategory(CategoryTable categoryTable) async {
    final db = await SQLHelper.db();

    final result = await db.update(
        'categories', categoryTable.toMap(),
        where: "categoryId = ?",
        whereArgs: [categoryTable.categoryId]
    );

    return result;
  }

  //Delete category item
  static Future<void> deleteCategory(int id) async {
    final db = await SQLHelper.db();

    try{
      await db.delete('categories', where: "categoryId = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint('Something went wrong when deleting an item: $err');
    }
  }

  //create new priority item
  static Future<int> createPriority(PriorityTable priorityTable) async {
    final db = await SQLHelper.db();
    final priorityId = await db.insert('priorities', priorityTable.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return priorityId;
  }

  //Read all priority items List-> obj
  static Future<List<PriorityTable>> getAllPriorities() async {
    final db = await SQLHelper.db();
    List<Map<String, dynamic>> allPriorityRows = await db.query('priorities');
    List<PriorityTable> priorities =
    allPriorityRows.map((priorityTable) => PriorityTable.fromMap(priorityTable)).toList();
    return priorities;
  }


  //Update priority item by id
  static Future<int> updatePriority(PriorityTable priorityTable) async {
    final db = await SQLHelper.db();

    final result = await db.update(
        'priorities', priorityTable.toMap(),
        where: "priorityId = ?",
        whereArgs: [priorityTable.priorityId]
    );

    return result;
  }

  //Delete priority item
  static Future<void> deletePriority(int id) async {
    final db = await SQLHelper.db();

    try{
      await db.delete('priorities', where: "priorityId = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint('Something went wrong when deleting an item: $err');
    }
  }

  //create new status item
  static Future<int> createStatus(StatusTable statusTable) async {
    final db = await SQLHelper.db();
    final statusId = await db.insert('status', statusTable.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return statusId;
  }

  //Read all status items List-> obj
  static Future<List<StatusTable>> getAllStatus() async {
    final db = await SQLHelper.db();
    List<Map<String, dynamic>> allStatusRows = await db.query('status');
    List<StatusTable> status =
    allStatusRows.map((statusTable) => StatusTable.fromMap(statusTable)).toList();
    return status;
  }


  //Update status item by id
  static Future<int> updateStatus(StatusTable statusTable) async {
    final db = await SQLHelper.db();

    final result = await db.update(
        'status', statusTable.toMap(),
        where: "statusId = ?",
        whereArgs: [statusTable.statusId]
    );

    return result;
  }

  //Delete status item
  static Future<void> deleteStatus(int id) async {
    final db = await SQLHelper.db();

    try{
      await db.delete('status', where: "statusId = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint('Something went wrong when deleting an item: $err');
    }
  }

  //create new note item
  static Future<int> createNote(Note note) async {
    final db = await SQLHelper.db();
    final noteId = await db.insert('notes', note.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return noteId;
  }

  //Read all note items List-> obj
  static Future<List<Note>> getAllNotes() async {
    final db = await SQLHelper.db();
    List<Map<String, dynamic>> allNoteRows = await db.query('notes');
    List<Note> note =
    allNoteRows.map((note) => Note.fromMap(note)).toList();
    return note;
  }


  //Update note item by id
  static Future<int> updateNote(Note note) async {
    final db = await SQLHelper.db();

    final result = await db.update(
        'notes', note.toMap(),
        where: "noteId = ?",
        whereArgs: [note.noteId]
    );

    return result;
  }

  //Delete note item
  static Future<void> deleteNote(int id) async {
    final db = await SQLHelper.db();

    try{
      await db.delete('notes', where: "noteId = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint('Something went wrong when deleting an item: $err');
    }
  }

  static Future<int> createItem(Account item) async {
    final db = await SQLHelper.db();

    final id = await db.insert(
        'accounts',
        item.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace); //trường hợp khóa chính bị trùng thì thay thế
    return id;
  }

  static Future<List<Map<String, dynamic>>> getItem() async{
    final db = await SQLHelper.db();

    return db.query('accounts', orderBy: 'id');
  }

  static Future<List<Map<String, dynamic>>> checkAccount(Account account) async{
    final db = await SQLHelper.db();

    return db.query('accounts',
        where: "email = ? and password = ?",
        whereArgs: [account.email, account.password],
        limit: 1);
  }
  //update Profile
  static Future<List<Map<String, Object?>>> updateProfile(String firstname, String lastname) async {
    final dbClient = await SQLHelper.db();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefsEmail = prefs.getString('getEmail') ?? "";
    // final res = await dbClient.update('accounts', acc.toMap(),
    //     where: 'email = ?', whereArgs: [prefsEmail]);

    final res = await dbClient.rawQuery('''UPDATE accounts SET firstname = '$firstname', lastname = '$lastname' where email = '$prefsEmail' ''');
    return res;
  }

  static Future<List<Map<String, Object?>>> updatePassword(String newPassword) async {
    final dbClient = await SQLHelper.db();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    final prefsEmail = prefs.getString('getEmail') ?? "";
    // final res = await dbClient.update('accounts', acc.toMap(),
    //     where: 'email = ?', whereArgs: [prefsEmail]);

    final res = await dbClient.rawQuery('''UPDATE accounts SET password = '$newPassword' where email = '$prefsEmail' ''');
    return res;
  }


}
