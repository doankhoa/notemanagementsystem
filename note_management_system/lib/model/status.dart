  class StatusTable{
  int? statusId;
  String? statusName;
  String? email;
  DateTime? createdAt;

  StatusTable({this.statusId, this.statusName, this.email, this.createdAt});

  StatusTable.fromMap(dynamic obj) {
    statusId = obj['statusId'];
    statusName = obj['statusName'];
    email = obj['email'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic> {
      'statusId': statusId,
      'statusName': statusName,
      'email': email,
    };

    return map;
  }
}