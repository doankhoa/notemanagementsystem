class PriorityTable{
  int? priorityId;
  String? priorityName;
  String? email;
  DateTime? createdAt;

  PriorityTable({this.priorityId, this.priorityName, this.email, this.createdAt});

  PriorityTable.fromMap(dynamic obj) {
    priorityId = obj['priorityId'];
    priorityName = obj['priorityName'];
    email = obj['email'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic> {
      'priorityId': priorityId,
      'priorityName': priorityName,
      'email': email,
    };

    return map;
  }
}