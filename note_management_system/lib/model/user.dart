class User{
  final int? idUser;
  final String? email;
  final String? pass;

  User({this.idUser , this.email, this.pass});

  Map<String, dynamic> toMap(){
    return {
      'idUser': idUser,
      'email' : email,
      'pass' : pass,
    };
  }
}