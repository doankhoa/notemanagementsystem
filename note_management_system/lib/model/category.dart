class CategoryTable{
  int? categoryId;
  String? categoryName;
  String? email;
  DateTime? createdAt;

  CategoryTable({this.categoryId, this.categoryName, this.email, this.createdAt});

  CategoryTable.fromMap(dynamic obj) {
    categoryId = obj['categoryId'];
    categoryName = obj['categoryName'];
    email = obj['email'];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic> {
      'categoryId': categoryId,
      'categoryName': categoryName,
      'email': email,
    };

    return map;
  }
}