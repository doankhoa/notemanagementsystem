class Note{
  int? noteId;
  String? noteName;
  String? email;
  String? planDate;
  String? createdAt;
  String? category;
  String? priority;
  String? status;

  Note({
    this.noteId , this.noteName, this.email, this.planDate, this.createdAt, this.category,
    this.priority, this.status ,
  });

  Note.fromMap(dynamic obj) {
    noteId = obj['noteId'];
    noteName = obj['noteName'];
    email = obj['email'];
    planDate = obj['planDate'];
    createdAt = obj['createdAt'];
    category = obj['FK_notes_category'];
    priority = obj['FK_notes_priority'];
    status = obj['FK_notes_status'];
  }

  Map<String, dynamic> toMap(){
    var map = <String, dynamic> {
      'noteId': noteId,
      'noteName' : noteName,
      'email': email,
      'planDate' : planDate,
      'createdAt' : createdAt,
      'FK_notes_category' : category,
      'FK_notes_priority': priority,
      'FK_notes_status' : status,
    };

    return map;
  }
}