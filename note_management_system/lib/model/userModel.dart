class Account{
  final int? id;
  final String? email;
  final String? password;
  final String? firstname;
  final String? lastname;
  final String? createAt;

  Account({this.id, this.email, this.password,this.firstname, this.lastname, this.createAt});

  Map<String, dynamic> toMap(){
    return {
      'id': id,
      'email':email,
      'password': password,
      'firstname':firstname,
      'lastname': lastname,
    };
  }
}